FROM python:latest

WORKDIR /app

COPY ./app/main.py .

ENTRYPOINT ["python", "main.py"]