import random
import sys
def generate_random_array(size):
    return sorted([random.randint(1, 100) for _ in range(size)])
def binary_search(arr, target):
    low = 0
    high = len(arr) - 1
    while low <= high:
        mid = (low + high) // 2
        if arr[mid] == target:
            return mid
        elif arr[mid] < target:
            low = mid + 1
        else:
            high = mid - 1

    return -1
if __name__ == "__main__":
    target = int(sys.argv[1])
    array = generate_random_array(100)
    print("Generated array:", array)

    index = binary_search(array, target)
    if index != -1:
        print(f"Found target value {target} at index {index}.")
    else:
        print(f"Target value {target} not found in the array.")